
import React, { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [redditData, setRedditData] = useState([]);
  const redditUrl = "https://www.reddit.com/r/reactjs.json";

  useEffect(() => {
    axios
      .get(redditUrl)
      .then((response) => {
        const data = response.data.data.children;
        setRedditData(data);
      })
      .catch((error) => {
        console.error("Error fetching data from Reddit:", error);
      });
  }, []);

  return (
    <div className="App">
      <h1>Reddit ReactJS Posts</h1>
      <div className="card-container">
      <main className="d-flex flex-wrap overflow-auto" style={{height:'50rem'}}>
        {redditData.map((post, index) => (
          <div className="card m-2 p-2" style={{width:'40rem'}} key={index}>
            <h2 className="title">{post.data.title}</h2>
            <div
              className="selftext"
              dangerouslySetInnerHTML={{ __html: post.data.selftext_html }}
            />
            <a
              className="url"
              href={post.data.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              {post.data.url}
            </a>
            <p className="score">Score: {post.data.score}</p>
          </div>
        ))}
        </main>
      </div>
    </div>
  );
}

export default App;
